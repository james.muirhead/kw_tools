#!/usr/bin/env/python2
# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module provides scripts to get all groups (AGS entitlements)
# associated with every Klocwork project  Output is a CSV file of the format:
# PROJECT, GROUP, ROLE.
# Additionally there is a method to update a Klocwork project's permissions,
# is to add/remove a group to/from the Klocwork project.


# Standard library imports
import csv
import json
import logging
import os
import sys
import urllib
import urllib2
from argparse import ArgumentParser
from urllib2 import HTTPError, URLError

# Application imports
from common import kw_common
from common.constants import Constants

# Module logger and Root logger init
logger = logging.getLogger('PERMISSIONS')
logging.basicConfig(stream=sys.stdout, format='%(name)s - %(message)s',
                    level=logging.DEBUG)


def main():
    """
    Klocwork group permissions functionality 'main' method.

    @return (int)  Non-zero on failure.
    """

    parser = ArgumentParser()
    parser.add_argument('-u', '--user',
                        default=Constants.kw_qb_user,
                        help='Klocwork user id',
                        dest='user')
    parser.add_argument('-l', '--url',
                        default=Constants.kw_url,
                        help='Full URL of Klocwork server, including the port.',
                        dest='url')
    subparser_help = '''
    dump: Dump role permissions to project CSV files.
    apply: Apply KW project permissions from CSV file.
    '''
    subparsers = parser.add_subparsers(description='Create group CSV '
                                                   'or update project permissions.',
                                       help=subparser_help,
                                       dest='update_type')
    update_parser = subparsers.add_parser('apply')
    update_parser.add_argument('-p', '--projects', dest='projects', default=None,
                               required=False,
                               help='Comma separated list of Klocwork project IDs.')
    update_parser.add_argument('-r', '--revoke', action='store_true',
                               dest='revoke', default=False)

    subparsers.add_parser('dump')

    args = parser.parse_args()

    ret_val = 1
    user = args.user
    url_list = args.url.split(':')
    port = url_list[-1]
    host = url_list[1].split('//')[1]
    update_type = args.update_type

    try:
        if update_type == 'dump':
            write_project_csv(get_kw_groups(host, port, user))
        elif update_type == 'apply':
            update_project_permissions(host, port, user, args.projects, args.revoke)

    # Basic Exception Handling
    except HTTPError as e:
        logger.error('HTTP Error {}'.format(e.code))
        logger.exception(e)

    except (URLError, OSError, IOError, ValueError) as e:
        logger.exception(e)

    except Exception as e:
        logger.error('Unhandled Exception')
        logger.exception(e)

    else:
        ret_val = 0

    return ret_val


def get_kw_groups(host, port, user):
    """
    Reads the Klocwork role list and retrieves all groups and users
    granted the role. Groups will be AGS entitlements.

    @param host:            KW server host name
    @param port:            KW server port
    @param user:            KW user name
    @return: (dictionary)   Dictionary of dictionaries containing KW group meta-data
    """

    # Dictionary that will be JSON payload for POST operation.
    data = {'action': 'role_assignments', 'user': user}
    # List of Klocwork roles; this is a constant.
    kw_role_list = Constants.kw_roles

    ltoken = kw_common.get_token(host, port, user)
    data['ltoken'] = ltoken
    api_url = 'https://{}:{}/review/api'.format(host, port)
    # Dictionary that will be returned
    group_dict = {}

    for kw_role in kw_role_list:
        # KW ReST API search param; the '+' means exact match.
        data['search'] = '+{}'.format(kw_role)
        encoded_data = urllib.urlencode(data)
        request = urllib2.Request(api_url, encoded_data)
        response = urllib2.urlopen(request)

        role = json.load(fp=response, encoding='utf-8')
        assignments = role['assignments']
        for assignment in assignments:
            # Make sure we have a group. 'group' will be True if
            # if 'name' is a group; it will be False if 'name' is
            # a user. 'group' is a boolean in JSON.
            if assignment['group']:
                # 'projectId' will not be present if the group
                # is assigned the role at the root, i.e. for every project.
                # We don't want to double-add these 'root' groups.
                if 'projectId' in assignment:
                    project_id = assignment['projectId']
                    group_name = assignment['name']
                    assignment_dict = {'group': group_name, 'role': kw_role}
                    if project_id not in group_dict:
                        group_dict[project_id] = [assignment_dict]
                    else:
                        group_dict[project_id].append(assignment_dict)
            else:
                # Just log any individual user assignments.
                logger.info('Individual user permission found...')
                logger.info('User Name: {} Project: {}'
                            .format(assignment['name'], assignment['projectId']))

    return group_dict


def write_project_csv(project_groups, omit_it=True):
    """
    Writes each project's group permissions (roles) to a separate CSV file.
    These files can later be used to update a given project's permissions.

    @param project_groups:  Dictionary of KW group/project meta-data
    @param omit_it:         If true, omit any IT KW groups (start with 'KWIT_IT')
    @return: (void)
    """
    groups_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'groups')
    if not os.path.exists(groups_dir):
        os.makedirs(groups_dir)

    for project_id, group_list in project_groups.items():
        # Generate CSV file name
        csv_file_name = os.path.normpath(
            os.path.join(groups_dir, '{}_groups.csv'.format(project_id)))
        # Open the CSV file for writing and insert a line for each group.
        # File will be overwritten if it exists.
        with open(csv_file_name, mode='w+') as f:
            writer = csv.writer(f, delimiter=',', quotechar='"',
                                quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
            # Insert CSV header row
            writer.writerow(['PROJECT', 'GROUP', 'ROLE'])
            for group in group_list:
                # Exclude IT KW admin groups if 'omit_it' is True.
                if not (omit_it and group['group'].startswith('KWIT_IT')):
                    writer.writerow([project_id, group['group'], group['role']])


def update_project_permissions(host, port, user, project_list=None, revoke=False):
    """
    Update KW project access.  Read a CSV file for a given project
    and update the project's permissions to match what is in the CSV file.

    Each record in the CSV file has the format:
    <PROJECT NAME>,<AGS Entitlement>,<KW ROLE>

    This method will either grant the AGS Entitlement the KW ROLE specified
    in the CSV record, or revoke access to the KW ROLE for the AGS Entitlement.

    A project CSV file may be generated with the 'write_project_csv' method in this
    module, or by hand.  The CSV file must have a header row, the records must match
    the format above, and the file name must be '<KW project id>_groups.csv'.

    @param host:            KW server host name
    @param port:            KW server port
    @param user:            KW user name
    @param project_list:    (Optional) Semi-colon separated List of Klocwork projects.
                            If None parse all CSVs in directory.
    @param revoke           Boolean, True to revoke permissions, False to add.
    @return: (void)
    """
    groups_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'groups')
    revoke_perms = 'false' if not revoke else 'true'
    perm_action = 'Updating' if not revoke else 'Revoking'
    if project_list is None:
        file_list = [f for f in os.listdir(groups_dir) if f.endswith('.csv')]
    else:
        file_list = ['{}{}'.format(p, '_groups.csv').strip()
                     for p in project_list.split(',')]
    api_url = 'https://{}:{}/review/api'.format(host, port)
    ltoken = kw_common.get_token(host, port, user)
    # Dictionary that will be JSON payload for POST operation.
    data = {'action': 'update_role_assignment', 'user': user,
            'ltoken': ltoken, 'group': 'true', 'remove': revoke_perms}

    for file_name in file_list:
        perms_file = os.path.join(groups_dir, file_name)
        with open(perms_file, mode='r') as f:
            reader = csv.reader(f, delimiter=',', quotechar="'")
            # Skip CSV file column headings.
            next(reader, None)
            for entry in reader:
                data['project'], data['account'], data['name'] = entry
                logger.info('{} - Project: {} Group: {} Role: {}'
                            .format(perm_action, data['project'],
                                    data['account'], data['name']))
                encoded_data = urllib.urlencode(data)
                request = urllib2.Request(api_url, encoded_data)
                urllib2.urlopen(request)


# Entry point
if __name__ == '__main__':
    exit_code = main()
    exit(exit_code)
