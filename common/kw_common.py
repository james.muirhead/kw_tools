# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module contains functionality necessary to several scripts
# in this project.
# get_token() retrieves the ltoken hash for the specified user.
# get_projects() get as list of all projects from the KW server ReST API.

# Standard library imports
import json
import logging
import os
import urllib
import urllib2

logger = logging.getLogger('COMMON')


def get_token(host, port, user):
    """
    Retrieves and returns the proper Klocwork 'ltoken' for the
    given Klocwork host, port, and user combination.
    The 'ltoken' is stored in the '.klocwork/ltoken' file within
    the user's home directory. Any given user on a
    a machine may have multiple entries in the ltoken file.
    Each entry is a record on a single line with
    the following format: Klocwork hostname;Klocwork host port;
    Klocwork username;Klocwork ltoken
    The ltoken is required for authentication into the Klocwork ReST API.

    'token' for test and debug:
    sys_nsg_atlas: '175b95b2b525b5f58aa216f4fcc3890275b909b539dd52a4055887de8c6758c5'
    sys_nsgqbagt: '602bd0919492b92dcae231a184004ab1400789b547196b6724c3085ce7d53423'

    @param host:        Klocwork server hostname
    @param port:        Klocwork server host's port
    @param user:        Klocwork username
    @return: (string)   Klocwork access token
    """
    # Index position of each field of a record in the ltoken file.
    ltoken_record_map = {'host': 0, 'port': 1, 'user': 2, 'token': 3}
    ltoken = None
    logger.info('Retrieving Klocwork access token...')

    try:
        # Get the OS-agnostic path to the user's home
        # directory and the ltoken file. It will always
        # be in the '.klocwork' sub-directory.
        ltoken_file = os.path.normpath(os.path.expanduser("~/.klocwork/ltoken"))
        assert os.path.exists(ltoken_file)
        # Open the ltoken file for 'read' and parse each line (record) of the file.
        # If the host, port, and username match those passed into the function,
        # then we are on the correct record.
        # In the vast majority of our cases there will only be a single
        # record in each ltoken file.
        with open(ltoken_file, mode='r') as ltf:
            for record in ltf:
                # 'kwauth' creates records with no spaces or trailing semi-colons.
                # But, the ltoken file can be manually edited, copied, etc.
                # Just to be safe, remove any whitespace and trailing semi-colons.
                record_array = record.replace(' ', '').rstrip(';\n\r').split(';')
                if record_array[ltoken_record_map['host']] == host \
                        and record_array[ltoken_record_map['port']] == str(port) \
                        and record_array[ltoken_record_map['user']] == user:
                    ltoken = record_array[ltoken_record_map['token']]
                    break

        assert ltoken is not None

        return ltoken

    except AssertionError:
        msg = '''
        get_token: 
        The Klocwork authentication token may not be empty.
        Make sure the "ltoken" file exists in ~/.klocwork/.
        Check that the actual Klocwork server FQDN is being used; not a DNS alias.
        Check that user "{}" has the correct entry in "~/.klocwork/ltoken".
        '''.format(user)
        logger.error('Assertion Error in get_token() method.\n{}'.format(msg))
        raise


def get_projects(user, ltoken, api_url):
    """
    Creates a list of dictionaries by querying the KW Web API.
    Each dictionary contains the project 'name' and 'id'.
    The project name is used in most cases, but the project
    id is required for accessing project permissions.

    @param user:                KW user name
    @param ltoken:              KW access token
    @param api_url:             KW web api url
    @return: (list)             List of dictionaries
                                {project name, project id}
    """
    project_list = []
    # Dictionary of data that is posted to KW ReST API.
    data = {'action': 'projects', 'user': user, 'ltoken': ltoken}

    # Encode the POST data and hit KW ReST API.
    encoded_data = urllib.urlencode(data)
    request = urllib2.Request(api_url, encoded_data)
    response = urllib2.urlopen(request)

    # Loop through the response adding each project name to list.
    for project in response:
        project = json.loads(project, encoding='utf-8')
        logger.info('Adding project {}...'.format(project['name']))
        project_list.append({'name': project['name'], 'id': project['id']})

    return project_list


def get_roles(user, ltoken, api_url):
    """
    Fetches role list from Klocwork.

    @param user:        Klocwork user name
    @param ltoken:      'ltoken' for Klocwork user
    @param api_url:     Klocwork Web API url
    @return: (list)     String list of Klocwork roles
    """
    logger.info('Retrieving role list from {}...'.format(api_url))
    role_list = []
    data = {'action': 'roles', 'user': user, 'ltoken': ltoken}
    # Encode the POST data and hit KW ReST API.
    encoded_data = urllib.urlencode(data)
    request = urllib2.Request(api_url, encoded_data)
    response = urllib2.urlopen(request)

    # Loop through the response adding each role name to list.
    for role in response:
        role = json.loads(role, encoding='utf-8')
        if role['name'] != 'Projects root admin':
            logger.info('Adding role {}...'.format(role['name']))
            role_list.append(role['name'])

    return role_list
