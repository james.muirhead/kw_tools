## Klocwork Tools

This project contains tools for managing and/or administrating Klocwork.  It does not contain any scripts to do either a Desktop or Server scan.


* [Verify Klocwork Checkers](#verify-klocwork-checkers)
* [Update Klocwork Checkers](#update-klocwork-checkers)
	* [Apply SDL Checkers](#apply-sdl-checkers)
	* [Apply NSG Checkers](#apply-nsg-checkers)
	* [Sync With CSV File](#sync-with-csv-file)
* [Klocwork Permissions](#klocwork-permissions)
    * [Dump Role Members to File](#dump-role-members-to-file)
    * [Apply Permissions from File](#apply-permissions-from-file)
* [Update Klocwork Project Meta-Data](#update-klocwork-project-meta-data)
* [Migrate Klocwork Projects](#migrate-klocwork-projects)
* [Validate Klocwork Server Migration](#validate-klocwork-server-migration)

#### Summary
The tools included here can be run standalone, in an IDE -- such as PyCharm -- or in a DevOps tool like [Quickbuild](https://www.pmease.com/). All packages have been successfully tested in standalone mode and from within PyCharm.

The packages were developed using Python 2.7 because it is the only common Python version on all of our QuickBuild build agents.  Porting to Python 3.6 or 3.7 should be fairly straightforward.  Updating to the Python 3.x `urllib` module is probably the only required change.

The source is pretty thoroughly commented.  If something is not covered below, it is likely covered in source comments.

#### Common Command Line Arguments
The following command line arguments are common to all use-cases.  All of these arguments are optional, and have a default value. If used, these arguments must be first on the command line, and come _before_ the sub-parser argument.

Please see the source code for default values.

`-u --user`&emsp;The Klocwork user name.

`-l --url`&emsp;The full Klocwork URL, including the port number.


<a name="verify-klocwork-checkers"></a>
#### Verify Klocwork Checkers
The `checkers` package contains functionality to download all Klocwork 'checkers' via the Klocwork Web API, and compare the status (enabled or disabled) of each 'checker' to its status from the previous checker verification run. In this way any changes to Klocwork 'checkers' can be captured.  A single Klocwork project name may optionally be added to command line.  If a project name is provided only that single project will be verified.

##### Verify Klocwork Checkers Command Line Arguments
`verify`&emsp;Sub-parser argument to invoke Klocwork checker validation.

`-p --project`&emsp;Optional name of single Klocwork project to validate. 

`-c --compare`&emsp;Whether or not to do a checker status comparison with previous run. May be either 'y' or 'n'.  If set to 'n', then checkers will be downloaded and a CSV zip archive created, but no comparison will be done. This value should only really ever be 'n' when doing an initial 'seed' run.  That is, creating an initial set of checker CSV files for the _next_ run to use for comparison.

##### Verify Klocwork Checkers Example Command Lines
_Using default values for common command line arguments, and optional single project:_<br />
`python kw_checkers.py verify -p Cliffdale_Refresh_Refresh_ssddev -c y`

_Using default values for common command line arguments, and comparing all projects:_<br />
`python kw_checkers.py verify -c y`


<a name="update-klocwork-checkers"></a>
#### Update Klocwork Checkers
The `checkers` package contains three pieces of functionality:
* Apply the Intel IT Security (IPAS) required SDL checkers to one or a list of Klocwork projects.
	* `sdl` sub-parser command line argument
* Apply the NSG specific checkers to one or or a list of Klocwork projects.
	* `nsg` sub-parser command line argument
* Sync a Klocwork project with a checker list stored in a CSV file.
	* `sync` sub-parser command line argument.

<a name="apply-sdl-checkers"></a>
##### Apply SDL Checkers
Using the `sdl` argument will cause one or all Klocwork projects to be updated with the Intel IT Security required SDL checker set.  If a single project name is not specified as a command line argument, then all projects are updated.  The set of checkers to be applied is contained in the 'sdl_checkers_list.csv' file.  This checker set is provided by Intel IT Security, and is currently at this UNC location:&ensp;_\\\klocwork.intel.com\kw_uploads\klocwork\sdl_.  Care should be taken to ensure the latest SDL checker list is used, and that it matches the version of Klocwork being used.

##### Apply SDL Checkers Command Line Arguments
`sdl`&emsp;Sub-parser argument to invoke Klocwork SDL checker update.

`-p --projects`&emsp;Optional, a comma separated list of Klocwork projects to update. 

##### Apply SDL Checkers Example Command Line
_Using default values for common command line arguments, and optional project list:_<br />
`python kw_checkers.py sdl -p Cliffdale_Refresh_Refresh_ssddev,ADS_Bootloader_ssddev`

_Using default values for common command line arguments, and updating all projects:_<br />
`python kw_checkers.py sdl`

<a name="apply-nsg-checkers"></a>
##### Apply NSG Checkers
Using the `nsg` argument will cause a user-supplied list of Klocwork projects to have NSG-specific checkers applied for a specified CSV file.

##### Apply NSG Checkers Command Line Arguments
`nsg`&emsp;Sub-parser argument to invoke Klocwork checker update.

`-p --projects`&emsp;Required, a comma separated list of Klocwork projects to update. 

`-c` `--csv`&emsp;Optional, CSV file containing checkers to be applied. See source for default value.

##### Apply NSG Checkers Example Command Line
_Using default values for common command line arguments, and list with single project:_<br />
`python kw_checkers.py nsg -p Cliffdale_Refresh_Refresh_ssddev`

<a name="sync-with-csv-file"></a>
##### Sync With CSV File
Using the `sync` argument will cause a single Klocwork project to have its checker status synchronized with a checker list stored in a CSV file.  The records in the CSV file must be of the form:  _CODE, NAME, ENABLED, SEVERITY_.  The file must also contain a heading row with these values.
* CODE:     Klocwork checker code.
* NAME:     Klocwork checker name.
* ENABLED:  Klocwork checker enabled status. A string of either 'True' or 'False'.
* SEVERITY: Klocwork checker severity. A string of either '1', '2', '3', or '4'. A value of '1' indicates a critical checker.

##### Sync With CSV File Command Line Arguments
`sync`&emsp;Sub-parser argument to invoke Klocwork checker CSV file synchronization.

`-c --csv`&emsp;CSV file to synchronize with.

`-p --project`&emsp;Name of single Klocwork project to synchronize. 

##### Sync With CSV File Example Command Line
_Using default values for common command line arguments,:_<br />
`python kw_checkers.py sync -c checker_file.csv -p Cliffdale_Refresh_Refresh_ssddev`

<a name="klocwork-permissions"></a>
#### Klocwork Permissions
The `kw_permissions` module provides functionality to 'dump' Active Directory group role membership for each Klocwork project.  Output is a CSV file for each project.  In addition, permissions can be updated -- added or revoked -- for a project based on a CSV file. 

<a name="dump-role-members-to-file"></a>
##### Dump Role Members to File
Using just the `dump` sub-parser argument all Klocwork roles and the Active Directory groups belonging to that role will be output to CSV files; one file per Klocwork project.  The output file name will be [project name]_groups.csv. The output can then be used to apply or revoke permissions to/from a Klocwork project.

The output CSV file will have PROJECT, GROUP, and ROLE fields.
* PROJECT:  Klocwork project name
* GROUP:    Active Directory group (AGS entitlement) 
* ROLE:     Klocwork role for Active Directory group

##### Dump Role Members to File Command Line Arguments
`dump`&emsp;Sub-parser argument to output Active Directory group role membership for each project to a CSV file.

##### Dump Role Members to File Example Command Line
_Using default values for common command line arguments:_<br />
`python kw_permissions.py dump`

<a name="apply-permissions-from-file"></a>
##### Apply Permissions from File
The `apply` argument will apply permissions (add/remove Active Directory groups to/from Klocwork roles) to a list of Klocwork projects based on the contents of a CSV file.  The CSV file is expected to be of the same format as the 'dump' files created above.

##### Apply Permissions from File Command Line Arguments
`apply`&emsp;Sub-parser argument to add/revoke permissions to/from a Klocwork project based on project CSV file.

`-p --projects`&emsp;Comma separated list of Klocwork project names.

`-r --revoke`&emsp;If present, revoke permissions instead of adding them.

##### Apply Permissions from File Example Command Line
_Using default values for common command line arguments:_<br />
`python kw_permissions.py apply -p Cliffdale_Refresh_Refresh_ssddev,ADS_Bootloader_ssddev`

<a name="update-klocwork-project-meta-data"></a>
#### Update Klocwork Project Meta-Data
The `kw_update_project` module allows for the updating of Klocwork project meta-data. Users may update any or all of the following:

The project description:  Typically the project description will contain the source code repository name or url, the source code branch, the compiler version, and the build target. This is a free text HTML `TextField`.

The number of Klocwork builds (scans) to be retained. Options are: 1, 5, 10, 15. The default is 10.

The project 'tags'. This is a comma separated list of strings applied to a project that make filtering the Web UI project list less cumbersome.
##### Update Klocwork Project Meta-Data Command Line Arguments
`-u --user`&emsp;The Klocwork user name. Optional, the default is the Quickbuild service faceless account user.

`-l --url`&emsp;The full Klocwork URL, including the port number. Optional, the default is: `https://nsg-kw.intel.com:8230`

`-p --project`&emsp;The name of the Klocwork project to be updated.

`-b --builds`&emsp;The number of Klocwork builds (scans) to retain.

`-t --tags`&emsp;Comma separated list of project 'tags'.

`-d --description`&emsp;The Klocwork project's description.
##### Update Klocwork Project Meta-Data Command Line Example
`python kw_update_project.py -u kwusername -l https://nsg-kw.intel.com:8230 -p ADS_Bootloader_ssddev -b 15 -t tag1,tag2,tag3 -d "project description"`
<a name="migrate-klocwork-projects"></a>
#### Migrate Klocwork Projects
The `kw_migrate_projects` module will migrate (copy) a Klocwork project, or projects, from one Klocwork server to another. All project information: builds, checker state, source references, and meta-data is copied. Please note that the user running this script _must_ be a **Projects Root Admin** on _both_ the source and destination Klocwork servers.

While the project is being exported it will be inaccessible on the source Klocwork server. Further, the project export is `forced`, so any user or system accessing the project at the time of export will be summarily disconnected.

Projects may be migrated one of two ways: either via file or web. In a file migration the Klocwork project is first exported from the source Klocwork server to an archive file, then the archive file is imported into the destination Klocwork server. After import the archive file is deleted because the archive files can be quite large.

A web migration uses the Klocwork Web API to copy the project directly from the source Klocwork server to the destination Klocwork server. An `import_project` Web API call is made to the destination Klocwork server's Web API.
##### Migrate Klocwork Projects Command Line Arguments
`-e --export_url`&emsp;Full URL, including port, of source Klocwork server.

`-i --import_url`&emsp;Full URL, including port, of destination Klocwork server.

`-p --projects`&emsp;Comma separated list of names of the Klocwork projects to be migrated.
###### Web API Project Migration
`web`&emsp;Sub-parser specifying a Web API project migration.

`-a --admin_user`&emsp;Projects Root Admin user name on source Klocwork server.

`-w --admin_pwd`&emsp;Source Klocwork server Projects Root Admin user's password.

`-d --dest_user`&emsp;Destination Klocwork server Projects Root Admin user name. The destination user is validated via the Klocwork _ltoken_ file.
###### File Project Migration
`file`&emsp;Sub-parser specifying a file project migration. No other arguments are necessary. The Python script will automatically generate file names based on the projects listed in the `--projects` command line argument.
<a name="validate-klocwork-server-migration"></a>
#### Validate Klocwork Server Migration
