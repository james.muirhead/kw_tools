#!/usr/bin/env/python2
# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module provides scripts to migrate (copy)
# Klocwork projects from one KW server to another.
# There are two options:
#    export the project to a file, then import from the file
#    import the project directly from one server to another via the KW Web API.


# Standard library imports
import logging
import os
import subprocess
import sys
import urllib
import urllib2
from argparse import ArgumentParser
from urllib2 import HTTPError, URLError

# Application imports
from common import kw_common
from util.kw_utils import parse_kw_url

# Logging init
logger = logging.getLogger('MIGRATE PROJECTS')
logging.basicConfig(stream=sys.stdout, format='%(name)s - %(message)s',
                    level=logging.DEBUG)


def main():
    """
    Klocwork project migration 'main' method.

    @return: (int)  Non-zero on failure.
    """
    parser = ArgumentParser()
    parser.add_argument('-e', '--export_url',
                        help='Full URL, including port, from which '
                             'projects will be exported.',
                        required=True,
                        dest='export_url')
    parser.add_argument('-i', '--import_url',
                        help='Full URL, including port, into which '
                             'projects will be imported.',
                        required=True,
                        dest='import_url')
    parser.add_argument('-p', '--projects',
                        help='Comma separated list of Klocwork project names.',
                        required=True,
                        dest='projects')
    subparsers = parser.add_subparsers(help='Migrate project via Klocwork Web API',
                                       dest='migrate_type')

    web_parser = subparsers.add_parser('web')
    web_parser.add_argument('-a', '--admin_user', dest='admin_user', required=True,
                            help='Migration source projects root admin user name.')
    web_parser.add_argument('-w', '--admin_pwd', dest='admin_pwd', required=True,
                            help='Migration source projects root admin user password.')
    web_parser.add_argument('-u', '--dest_user', dest='dest_user', required=True,
                            help='Migration destination admin user name.')

    file_type_parser = subparsers.add_parser('file')

    args = parser.parse_args()

    logger.info('Export URL: {}'.format(args.export_url))
    logger.info('Import URL: {}'.format(args.import_url))
    logger.info('Project List: {}'.format(args.projects))
    project_list = [p.strip() for p in args.projects.split(',')]
    ret_val = 1

    try:
        if args.migrate_type == 'file':
            logger.info('Migrating projects via "file" method...')
            migrate_projects_file(project_list, args.export_url, args.import_url)
        elif args.migrate_type == 'web':
            logger.info('Export Projects Root Admin User {}'.format(args.admin_user))
            logger.info('Migrating projects via "Web API" method...')
            migrate_projects_web_api(args.export_url, args.import_url, args.admin_user,
                                     args.admin_pwd, args.dest_user, project_list)
    # Basic Exception Handling
    except HTTPError as he:
        logger.error('HTTPError {}'.format(he.code))
        logger.exception(he)

    except (URLError, OSError, IOError, ValueError) as e:
        logger.exception(e)

    except Exception as e:
        logger.error('Unhandled Exception')
        logger.exception(e)

    else:
        ret_val = 0

    return ret_val


def migrate_projects_file(project_list, export_url, import_url):
    """
    Migrate Klocwork projects from one KW server to another by
    exporting the project to a file, then importing from that file.

    @param project_list:    List of KW project names to migrate
    @param export_url:      Project export url
    @param import_url:      Project import url
    @return (void)
    """
    for project in project_list:
        logger.info('Migrating {} project via export file...'.format(project))
        export_kw_project('{}/{}'.format(export_url, project), project)
        import_kw_project(import_url, project)
        # Delete each archive file because they can be quite large.
        delete_export_file(project)


def export_kw_project(kw_project_url, kw_project):
    """
    Export a Klocwork project to an archive file.

    @param kw_project_url:      Project export URL
    @param kw_project:          Name of KW project to be exported.
    @return (void)
    """
    export_file = '{}.export'.format(kw_project.replace(' ', '_'))
    cmd_str = 'kwprojcopy export --url {} {} --includeSources --force'\
        .format(kw_project_url, export_file)
    logger.info(cmd_str)
    subprocess.check_call(cmd_str, shell=True)


def import_kw_project(import_url, kw_project):
    """
    Import a Klocwork project from an archive file.  The project
    must not already exist on the import Klocwork server.

    @param import_url:  URL of Klocwork server to receive new project.
    @param kw_project:  Name of project to be imported.
    @return (void)
    """
    import_file = '{}.export'.format(kw_project.replace(' ', '_'))
    cmd_str = 'kwprojcopy import --url {} {}'.format(import_url, import_file)
    logger.info(cmd_str)
    subprocess.check_call(cmd_str, shell=True)


def delete_export_file(kw_project):
    """
    Deletes Klocwork project archive file; the file that results from a project export.
    @param kw_project:  Klocwork project name; also the name of the '.archive' file.
    @return (void)
    """
    export_file = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               '{}.export'.format(kw_project.replace(' ', '_')))
    if os.path.exists(export_file):
        logger.info('Deleting {}...'.format(export_file))
        os.remove(export_file)
    else:
        logger.warning('{} export file not found.'.format(export_file))


def migrate_projects_web_api(source_url, source_user, source_pwd,
                             dest_url, dest_user, project_list):
    """
    Migrate (copy) projects from one Klocwork server to another using the Web API.
    The importing Klocwork server will pull the project from the 'exporting' server.

    @param source_url:      URL of project being exported
    @param dest_url:        URL of Klocwork server importing the new project.
    @param source_user:     Projects root admin user from the exporting KW server
    @param source_pwd:      'source_user' password
    @param dest_user:       Projects root admin user from the importing server.
    @param project_list:    List of Klocwork project names to be migrated.
    @return (void)
    """
    api_url, host, port = parse_kw_url(dest_url)
    ltoken = kw_common.get_token(host, port, dest_user)
    data = {'action': 'import_project', 'user': dest_user, 'ltoken': ltoken,
            'sourceURL': source_url, 'sourceAdmin': source_user,
            'sourcePassword': source_pwd}

    for project in project_list:
        logger.info('Migrating {} project via Web API...'.format(project))
        data['project'] = project
        # POST the updates
        encoded_data = urllib.urlencode(data)
        request = urllib2.Request(api_url, encoded_data)
        urllib2.urlopen(request)


# Entry point
if __name__ == '__main__':
    exit_code = main()
    exit(exit_code)
