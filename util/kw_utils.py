# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# Module containing general utility methods.


def str2bool(s):
    """
    Converts a string to a boolean value.
    ** Careful here.  Anything other than what
    ** is in the list will be considered False.

    @param s:           String to evaluate for 'truthiness'.
    @return: (boolean)  True if argument in 'truthy' list; False otherwise.
    """
    return s.lower() in ('yes', 'y', 'true', 't', '1')


def normalize_kw_vars(kw_project, kw_url=None):
    """
    If the KW project name contains spaces, the project name
    and URL need to be 'normalized' prior to doing a scan;
    they need to surrounded by double-quotes (").
    In some cases only the project name may need to be normalized.

    @param kw_project:      Klocwork project name.
    @param kw_url:          Base Klocwork ReST API URL
    @return: (dictionary)   'project': Double-quoted KW project name
                            'url': Double-quoted KW ReST API URL
    """
    quote = '' if ' ' not in kw_project else '"'
    normalized_project = '{}{}{}'.format(quote, kw_project, quote)
    normalized_project_url = None

    if kw_url:
        normalized_project_url = '{}{}/{}{}'.format(quote, kw_url, kw_project, quote)

    return {'project': normalized_project, 'url': normalized_project_url}


def parse_kw_url(url):
    """
    Parses the Klocwork URL and returns the host, port, and Web API url.

    @param url:         Full Klocwork server url, including the port
    @return: (tuple)    Strings: KW Web API url, KW host name, KW port
    """
    api_url = '{}/review/api'.format(url)
    url_list = url.split(':')
    port = url_list[-1]
    host = url_list[1].split('//')[1]

    return api_url, host, port
