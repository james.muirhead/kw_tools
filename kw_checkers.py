#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# Standard library imports
import logging
import os
import shutil
import sys
from argparse import ArgumentParser
from urllib2 import HTTPError, URLError
from zipfile import BadZipfile, LargeZipFile

# Application imports
from checkers import kw_update_checkers, kw_verify_checkers
from common.constants import Constants
from util.kw_utils import parse_kw_url

logger = logging.getLogger('CHECKERS')
logging.basicConfig(stream=sys.stdout, format='%(name)s - %(message)s',
                    level=logging.INFO)


def main():
    """
    Called from script entry point.
    Parses the command line and determines what type of Klocwork
    checker 'job' has been requested.  Then the appropriate
    method in the appropriate package is invoked.

    @return: (int)  Non-zero on failure.
    """
    parser = ArgumentParser()
    parser.add_argument('-u', '--user',
                        default=Constants.kw_qb_user,
                        help='Klocwork user id',
                        dest='user')
    parser.add_argument('-l', '--url',
                        default=Constants.kw_url,
                        help='Full Klocwork URL including port',
                        dest='url')
    subparser_help = '''
    verify: Verify KW checkers have not changed.
    sdl: Update KW checkers with SDL defaults.
    nsg: Enable/disable checkers per NSG standards.
    sync: Sync checker status with those in CSV file.
    '''
    subparsers = parser.add_subparsers(dest='run_type', help=subparser_help,
                                       description='Type of KW checker job to run.')
    # Verify Klocwork checkers have not changed
    verify_parser = subparsers.add_parser('verify')
    verify_parser.add_argument('-p', '--project', default=None, dest='project',
                               help='Evaluate a single KW project')
    verify_parser.add_argument('-c', '--compare', default='y', dest='compare',
                               choices=['y', 'n'],
                               help='If yes ("y"), perform checker comparison')
    # Update Klocwork checkers with SDL (IPAS) default checkers
    sdl_parser = subparsers.add_parser('sdl')
    sdl_parser.add_argument('-p', '--projects', default=None, dest='projects',
                            required=False,
                            help='Comma separated list of KW project names')
    # Synchronize a Klocwork project's checkers using a CSV file
    sync_parser = subparsers.add_parser('sync')
    sync_parser.add_argument('-c', '--csv', dest='csv_file', required=True,
                             help='CSV file to sync checkers')
    sync_parser.add_argument('-p', '--project', dest='project', required=True,
                             help='Project to be synchronized')
    # Update Klocwork checkers to add NSG specific checkers
    nsg_parser = subparsers.add_parser('nsg')
    nsg_parser.add_argument('-p', '--projects',
                            dest='projects', required=True,
                            help='Comma separated list of KW project names')
    nsg_parser.add_argument('-c', '--csv', default='nsg_checker_list.csv',
                            dest='csv_file', required=False,
                            help='CSV file with NSG checkers')

    args = parser.parse_args()

    ret_val = 1
    user = args.user
    run_type = args.run_type
    url = args.url
    api_url, host, port = parse_kw_url(url)
    project = args.project if 'project' in args else None
    base_dir = os.path.dirname(os.path.realpath(__file__))

    try:
        # Verify Klocwork checkers have not changed.
        # This can be run for all projects --
        # project variable is None, or a single project.
        if run_type == 'verify':
            kw_verify_checkers.verify_kw_checkers(host, port, api_url, user, args.compare,
                                                  base_dir, project)
        # Apply default SDL checkers
        elif run_type == 'sdl':
            projects = args.projects.split(',')
            kw_update_checkers.update_sdl_checkers(host, port, api_url, user, projects)
        # Enable/disable NSG specific checkers
        elif run_type == 'nsg':
            projects = args.projects.split(',')
            kw_update_checkers.update_nsg_checkers(host, port, api_url, user,
                                                   projects, args.csv_file)
        # Synchronize checkers using specified CSV file
        elif run_type == 'sync':
            kw_update_checkers.sync_project_checkers(host, port, user, project,
                                                     base_dir, args.csv_file)

    # Basic Exception Handling
    except HTTPError as he:
        logger.error('HTTPError {}'.format(he.code))
        logger.exception(he)

    except (URLError, OSError, IOError, AssertionError, ValueError, BadZipfile,
            LargeZipFile, shutil.Error) as e:
        logger.exception(e)

    except Exception as e:
        logger.error('Unhandled Exception')
        logger.exception(e)

    else:
        ret_val = 0

    return ret_val


# Entry point
if __name__ == '__main__':
    exit_code = main()
    exit(exit_code)
