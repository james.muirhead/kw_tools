# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module provides scripts to do Klocwork checker validation.
# The current Klocwork checker set and their statuses can be validated
# against a list of checkers (and statuses) in a CSV file.


# Standard library imports
import csv
import difflib
import filecmp
import json
import logging
import os
import shutil
import urllib
import urllib2
import zipfile

# Application imports
from common import kw_common
from util import kw_utils

logger = logging.getLogger('VALIDATE CHECKERS')


def _get_checkers_from_kw(user, ltoken, api_url, current_dir, project_list):
    """
    For each KW project in the project list, get the current state of all checkers.
    Write the checker information for each project to a CSV file.
    @param user:            KW user name
    @param ltoken:          KW access token
    @param api_url:         KW ReST API url
    @param current_dir:     Directory containing current checker information.
    @param project_list:    List of KW project names
    @return: (void)
    """
    # Dictionary of data 'POSTed' to KW ReST API.
    data = {'action': 'defect_types', 'user': user, 'ltoken': ltoken}

    # Loop through project list fetching checkers for each project.
    for project in project_list:
        project_name = project['name']
        logger.info('Retrieving checkers for {}...'.format(project_name))

        # Add project name to POST data
        data['project'] = project_name

        # Encode the POST data and hit KW ReST API.
        encoded_data = urllib.urlencode(data)
        request = urllib2.Request(api_url, encoded_data)
        response = urllib2.urlopen(request)

        # Generate CSV file name
        csv_file_name = os.path.normpath(
            os.path.join(current_dir, '{}_checkers.csv'.format(project_name)))

        # Open the CSV file for writing and insert a line for each checker.
        # File will be overwritten if it exists.
        with open(csv_file_name, mode='w+') as checkers_csv:
            checker_writer = csv.writer(checkers_csv,
                                        delimiter=',',
                                        quotechar='"',
                                        quoting=csv.QUOTE_MINIMAL,
                                        lineterminator='\n')
            # CSV file header row
            checker_writer.writerow(['CODE', 'NAME', 'ENABLED', 'SEVERITY'])
            for checker in response:
                checker = json.loads(checker, encoding='utf-8')
                checker_writer.writerow([checker['code'],
                                         checker['name'],
                                         str(checker['enabled']),
                                         str(checker['severity'])])


def compare_checkers(project_list, current_dir, previous_dir, html_dir):
    """
    Compare the checkers from the current run to the checkers fetched
    from the previous run, or from a manually created CSV file.
    Note that the CSV file directory names are hard-coded for expediency:
    current:  CSV files from current run
    previous: CSV files from previous run

    @param project_list:    List of KW project names
    @param current_dir:     Directory with current checker CSV files.
    @param previous_dir:    Directory with checker CSV files from previous run.
    @param html_dir:        Directory to house HTML diff reports.
    @return: (void)
    """
    if os.path.exists(previous_dir):
        logger.info('Comparing current checkers with previous run...')
        # Loop through list of current KW project names.
        for project in project_list:
            project_name = project['name']
            # Checker CSV file name. Will be the same for both
            # current and previous runs.
            base_file = '{}_checkers.csv'.format(project_name)
            # Full path to current checker CSV file.
            current_file = os.path.normpath(os.path.join(current_dir, base_file))
            # Full path to checker CSV file from previous run.
            previous_file = os.path.normpath(os.path.join(previous_dir, base_file))
            # Previous checker file may be missing if this is a new project.
            if os.path.exists(previous_file):
                # Compare current and previous files; true if identical.
                if filecmp.cmp(previous_file, current_file, shallow=False):
                    logger.info('{} checkers identical'.format(project_name))

                else:
                    # File compare returned false, so create HTML diff file.
                    logger.info('{} checkers have changed.'.format(project_name))
                    logger.info('Creating HTML diff file...')

                    if not os.path.exists(html_dir):
                        os.mkdir(html_dir)

                    with open(previous_file, mode='r') as p:
                        prev_lines = p.readlines()

                    with open(current_file, mode='r') as c:
                        curr_lines = c.readlines()

                    # When viewing the HTML diff file:
                    # The current checker list will in in the left-hand pane;
                    # the previous checker list will be in the right-hand pane.
                    html_diff = difflib.HtmlDiff(tabsize=2) \
                        .make_file(curr_lines, prev_lines)

                    html_file = os.path.normpath(
                        os.path.join(html_dir, '{}_diff.html'.format(project_name)))
                    with open(html_file, mode='w+') as h:
                        h.write(html_diff)

            else:
                logger.info("Previous checker file for '{}' not found. Skipping..."
                            .format(project_name))

    else:
        raise IOError('Klocwork checker previous directory does not exist.')


def unzip_previous_checkers(previous_dir, zip_file):
    """
    Unzip the checker CSV file archive from previous QuickBuild run.
    This will be a QuickBuild build artifact.

    @param previous_dir:    Directory with checker CSV files from previous run.
    @param zip_file:        Zip archive file name.
    @return: (void)
    """
    # Open zip archive and extract all files.
    with zipfile.ZipFile(zip_file, mode='r') as checker_zip:
        checker_zip.extractall(previous_dir)


def zip_html_dir(html_dir):
    """
    Create compressed zip archive of all HTML diff files.

    @param html_dir:  Directory containing HTML diff files.
    @return: (void)
    """
    # Create zip archive
    shutil.make_archive(base_name='kw_html_diff', format='zip', root_dir=html_dir,
                        logger=logger)


def zip_checkers(current_dir):
    """
    Creates compressed zip archive of current checker CSV files.
    This .zip file will be published as a QuickBuild artifact.

    @param current_dir: Full path to directory with current CSV checker files.
    @return: (void)
    """
    # Create zip archive
    shutil.make_archive(base_name='klocwork_checkers', format='zip',
                        root_dir=current_dir, logger=logger)


def verify_kw_checkers(host, port, api_url, user, compare, base_dir, project=None):
    """
    Module entry point.
    This method will call the necessary methods to:
    Download current checkers for specified KW projects.
    Create a compressed zip archive of CSV files containing current checkers.
    Compare the current checker status with that of a previous run.

    @param host:        KW server host name
    @param port         KW server port number
    @param api_url      KW Web API url
    @param user:        KW user name
    @param compare:     Whether or not to do a checker compare. If 'n' then just
                        download checkers and create zip archive.
    @param base_dir:    Directory which contains 'current', 'previous', and 'html'
                        sub-directories, aa well as the checker zip archive.
                        By default will be 'kw_checkers.py' script directory.
    @param project:     If not not None, download/compare just this project.
    @return (bool)      True if successful, False otherwise.
    """

    # Initialize return value
    ret_val = False
    # Directory to contain HTML diff files
    html_dir = os.path.normpath(os.path.join(base_dir, 'html'))
    # Full path to current checker CSV file directory.
    current_dir = os.path.normpath(os.path.join(base_dir, 'current'))
    # Create the current directory if it does not exist.
    if not os.path.exists(current_dir):
        os.mkdir(current_dir)

    # Get the KW access token from the current user's ltoken file.
    ltoken = kw_common.get_token(host, port, user)
    # Get checkers for all KW projects, or just a single
    # KW project if project name is supplied.
    project_list = kw_common.get_projects(user, ltoken, api_url) \
        if project is None else [project]
    # Retrieve all checkers from KW ReST API for projects in 'project_list'.
    _get_checkers_from_kw(user, ltoken, api_url, current_dir, project_list)
    # Create compressed archive of current CSV checker files.
    zip_checkers(current_dir)
    # Check if we're doing a compare. If no compare then we're just
    # dumping the lists of all checkers for projects in 'project_list'.
    logger.info('Compare Checkers: {}'.format(compare.upper()))
    if kw_utils.str2bool(compare):
        # Full path to the directory with checker CSV files from previous run.
        previous_dir = os.path.normpath(os.path.join(base_dir, 'previous'))
        # CSV file compressed archive from last successful QB run.
        previous_zip_file = os.path.normpath(
            os.path.join(previous_dir, 'klocwork_checkers.zip'))
        # Make sure the zip archive from previous run exists.
        # If not, raise an exception.
        if os.path.exists(previous_zip_file):
            # Unzip previous run checker CSV files.
            unzip_previous_checkers(previous_dir, previous_zip_file)
            # Compare all the checker files from current and previous runs.
            compare_checkers(project_list, current_dir, previous_dir, html_dir)
            # If any HTML diff files where created,
            # generate a zip archive of them.
            if os.path.exists(html_dir):
                zip_html_dir(html_dir)

        else:
            raise IOError('Klocwork checker archive file not found.')

    ret_val = True

    return ret_val
