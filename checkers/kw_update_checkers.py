# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module will update the checkers on the KW server for a given set of projects.
# The update can be a 'sync' using a CSV file, or an 'update' in which the
# default SDL checkers are applied to a KW project.

# Standard library imports
import csv
import logging
import os
import urllib
import urllib2

# Application imports
from common import kw_common

logger = logging.getLogger('UPDATE CHECKERS')


def sync_project_checkers(host, port, api_url, user, project, base_dir, csv_file):
    """
    Sync a given project's checkers with those in the specified CSV file.

    @param host:        KW server host name
    @param port:        KW server port number
    @param api_url:     KW Web API url
    @param user:        KW user name
    @param project:     KW project to sync
    @param base_dir:    Directory which contains CSV checker file.
                        By default will be 'kw_checkers.py' script directory.
    @param csv_file:    CSV checker file to use for sync'ing
    @return: (boolean)  True if successful, False otherwise.
    """

    # Get the supplied user's Klocwork access token (ltoken)
    ltoken = kw_common.get_token(host, port, user)
    # Map of KW checker data field indices
    # checker_idx_map = {'code': 0, 'name': 1, 'enabled': 2, 'severity': 3}
    # Dictionary that will be POSTed to the KW ReST API
    data = {'action': 'update_defect_type', 'user': user,
            'ltoken': ltoken, 'project': project}
    # Method return value, False (failure) by default.
    ret_val = False

    logger.info('Updating checkers for {} Klocwork project...'.format(project))
    logger.info('Synchronizing with CSV File: {}'.format(csv_file))

    csv_file_path = os.path.realpath(os.path.join(base_dir, csv_file))

    with open(csv_file_path, mode='r') as checker_csv:
        checker_reader = csv.reader(checker_csv, delimiter=',', quotechar='"')
        # Skip CSV file column headings.
        next(checker_reader, None)
        for checker in checker_reader:
            data['code'], data['name'], data['enabled'], data['severity'] = checker

            logger.info('Updating - Code: {} Enabled: {} Severity: {}'
                        .format(data['code'], data['enabled'], data['severity']))

            # Encode the POST data and hit KW ReST API.
            encoded_data = urllib.urlencode(data)
            request = urllib2.Request(api_url, encoded_data)
            urllib2.urlopen(request)

        ret_val = True

    return ret_val


def update_nsg_checkers(host, port, api_url, user, projects,
                        checker_file='nsg_checker_list.csv'):
    """

    @param host:            KW server host name
    @param port:            KW server port number
    @param api_url:         KW Web API url
    @param user:            KW user name
    @param projects:        List of KW projects to update
    @param checker_file:    CSV file containing checkers to update.
    @return: (boolean)      True if successful, False otherwise.
    """

    ltoken = kw_common.get_token(host, port, user)
    # Payload to send in ReST POST
    data = {'action': 'update_defect_type', 'user': user, 'ltoken': ltoken}
    ret_val = False

    # Open the default SDL checker CSV file
    csv_path = os.path.join(os.path.dirname(__file__), checker_file)
    with open(csv_path, mode='r') as checker_csv:
        checker_reader = csv.reader(checker_csv, delimiter=',', quotechar='"')
        # Loop through each KW project in the list
        for project in projects:
            logger.info('Applying default NSG checkers to {} project...'
                        .format(project))
            # Add the project name to the ReST payload.
            data['project'] = project
            # Seek to first record in CSV SDL checker file.
            # There are no headers in this file to make things easier.
            checker_csv.seek(0)
            for checker in checker_reader:
                # Add the checker code and enabled status to ReST payload.
                # Only the 'enabled' property is updated.
                data['code'], _, data['enabled'], _ = checker

                logger.info('Updating - Code: {} Enabled: {}'
                            .format(data['code'], data['enabled']))

                # Encode the POST data and hit KW ReST API.
                encoded_data = urllib.urlencode(data)
                request = urllib2.Request(api_url, encoded_data)
                urllib2.urlopen(request)

        ret_val = True

    return ret_val


def update_sdl_checkers(host, port, api_url, user, projects):
    """
    Updates a single project or all projects, applying the default
    SDL checkers.
    @param host:        KW server host name
    @param port:        KW server port number
    @param api_url:     KW Web API url
    @param user:        KW user name
    @param projects:    List of kw projects to update.
    @return: (boolean)  True if successful, False otherwise.
    """

    ltoken = kw_common.get_token(host, port, user)
    # Payload to send in ReST POST
    data = {'action': 'update_defect_type', 'user': user, 'ltoken': ltoken}
    ret_val = False
    # Open the default SDL checker CSV file
    csv_path = os.path.join(os.path.dirname(__file__), 'sdl_checker_list.csv')
    with open(csv_path, mode='r') as checker_csv:
        checker_reader = csv.reader(checker_csv, delimiter=',', quotechar='"')
        # Loop through each KW project in the list
        for project in projects:
            logger.info('Applying default SDL checkers to {} project...'
                        .format(project))
            # Add the project name to the ReST payload.
            data['project'] = project
            # Seek to first record in CSV SDL checker file.
            # There are no headers in this file to make things easier.
            checker_csv.seek(0)
            for checker in checker_reader:
                # Add the checker code and enabled status to ReST payload.
                # Only the 'enabled' property is updated.
                data['code'], _, data['enabled'],  data['severity'] = checker
                logger.info('Updating - Code: {} Enabled: {} Severity: {}'
                            .format(data['code'],
                                    data['enabled'],
                                    data['severity']))

                # Encode the POST data and hit KW ReST API.
                encoded_data = urllib.urlencode(data)
                request = urllib2.Request(api_url, encoded_data)
                urllib2.urlopen(request)

        ret_val = True

    return ret_val
