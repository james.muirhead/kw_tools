# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================


class Constants(object):
    """
    Class that holds constants needed for various Klocwork operations.

    """

    kw_fqdn = 'nsg-kw.intel.com'
    kw_port = '8230'
    kw_scheme = 'https'
    kw_qb_user = 'sys_nsgqbagt'
    kw_roles = ['Manager', 'Developer', 'Build engineer', 'Project admin', 'Reporter']
    kw_url = '{}://{}:{}'.format(kw_scheme, kw_fqdn, kw_port)

    class ConstError(TypeError):
        pass

    def __setattr__(self, key, value):
        if key in self.__dict__:
            raise self.ConstError('Cannot rebind constant: {}'.format(key))

        self.__dict__[key] = value

    def __init__(self):
        pass
