#!/usr/bin/env/python2
# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module provides scripts to update Klocwork project meta-data.


# Standard library imports
import logging
import sys
import urllib
import urllib2
from argparse import ArgumentParser
from urllib2 import HTTPError, URLError

# Application imports
from common import kw_common
from common.constants import Constants
from util.kw_utils import parse_kw_url

logger = logging.getLogger('UPDATE PROJECT')
logging.basicConfig(stream=sys.stdout, format='%(name)s - %(message)s',
                    level=logging.INFO)


def main():
    """

    @return: (int)  Non-zero on failure.
    """
    parser = ArgumentParser()
    parser.add_argument('-u', '--user', default=Constants.kw_qb_user,
                        help='Klocwork user id', dest='user')
    parser.add_argument('-l', '--url', default=Constants.kw_url,
                        help='Full URL of Klocwork server, including the port.',
                        dest='url')
    parser.add_argument('-p', '--project', dest='project',
                        required=True, help='Klocwork project to update')
    parser.add_argument('-b', '--builds', default=None, dest='builds',
                        required=False, help='Number if KW builds to retain.')
    parser.add_argument('-t', '--tags', default=None, dest='tags',
                        required=False, help='Comma separated list of KW project tags.')
    parser.add_argument('-d', '--description', default=None, dest='description',
                        required=False, help='KW project description')

    logger.info('Parsing command line...')
    args = parser.parse_args()
    # Update project meta-data
    ret_val = 1
    project = args.project
    user = args.user
    description = args.description
    tags = args.tags
    builds = args.builds

    logger.info('KW Project: {}'.format(project))

    try:
        update_kw_project(args.url, user, project, description, tags, builds)

    # Basic Exception Handling
    except HTTPError as e:
        logger.error('HTTP Error {}'.format(e.code))
        logger.exception(e)

    except (URLError, OSError, IOError, AssertionError, ValueError) as e:
        logger.exception(e)

    except Exception as e:
        logger.error('Unhandled Exception')
        logger.exception(e)

    else:
        ret_val = 0

    return ret_val


def update_kw_project(url, user, project, description=None, tags=None, builds=None):
    """
    Updates Klocwork project meta data:
        The project description displayed in the web UI.
        The project search/filter tags.
        The number of Klocwork builds to retain.

    This will allow for dev teams to run a self-service QuickBuild
    configuration to update this data.

    @param url:         KW server full url including port
    @param user:        KW user name
    @param project:     KW project to update
    @param description: (Optional) Updated KW project description
    @param tags:        (Optional) Updated KW project tags
    @param builds:      (Optional) Number of KW builds to retain
    @return: (boolean)  True if successful, False on failure or no project changes.
    """
    ret_val = False
    # Dictionary of data 'POSTed' to KW ReST API.
    data = {'action': 'update_project', 'user': user, 'name': project}
    # Check if there is anything to update. If not, exit.
    if description or tags or builds:
        api_url, host, port = parse_kw_url(url)
        ltoken = kw_common.get_token(host, port, user)
        data['ltoken'] = ltoken
        # Add the KW project description, tags, or builds
        # to the POST data dictionary.
        if description:
            # The description string comes over from QuickBuild as
            # a 'raw' string with the backslash escaped.
            data['description'] = description.replace('\\n', '\n')

        if tags:
            data['tags'] = tags

        if builds:
            data['auto_delete_builds'] = 'true'
            data['auto_delete_threshold'] = builds
        # POST the updates
        encoded_data = urllib.urlencode(data)
        request = urllib2.Request(api_url, encoded_data)
        urllib2.urlopen(request)

        ret_val = True

    else:
        logger.warn('No changes requested to {} project. Skipping...'.format(project))

    return ret_val


# Entry point
if __name__ == '__main__':
    exit_code = main()
    exit(exit_code)
