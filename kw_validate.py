#!/usr/bin/env/python2
# -*- coding: utf-8 -*-

# ============================================================================
#
# INTEL CONFIDENTIAL
#
# Copyright 2017-2018 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.
#
# ============================================================================

# This module provides scripts to validate the full migration from
# one Klocwork server to another.


# Standard library imports
import json
import logging
import sys
import urllib
import urllib2
from argparse import ArgumentParser
from urllib2 import HTTPError, URLError

# Application imports
from common import kw_common, constants
from util.kw_utils import parse_kw_url

# Module logger and Root logger init
logger = logging.getLogger('VALIDATE')
logging.basicConfig(stream=sys.stdout, format='%(name)s - %(message)s',
                    level=logging.DEBUG)


def main():
    """
    Klocwork project migration validation 'main' method.

    @return: (int)  Non-zero on failure.
    """
    parser = ArgumentParser()
    parser.add_argument('-s', '--source_url',
                        help='Full URL, including port, of KW migration source.',
                        dest='source_url',
                        default='https://klocwork-jf32.devtools.intel.com:8230')
    parser.add_argument('-t', '--target_url',
                        help='Full URL, including port, of KW migration target.',
                        dest='target_url',
                        default='https://fmgnsgkwpub101.fm.intel.com:8230')
    parser.add_argument('-u', '--kw_user',
                        help='Klocwork user name used for validation.',
                        dest='kw_user',
                        default=constants.Constants.kw_qb_user)
    args = parser.parse_args()

    source_url = args.source_url
    source_api_url, source_host, source_port = parse_kw_url(source_url)

    target_url = args.target_url
    target_api_url, target_host, target_port = parse_kw_url(target_url)

    kw_user = args.kw_user

    logger.info('Source URL: {}'.format(source_url))
    logger.info('Target URL: {}'.format(target_url))
    ret_val = 1

    all_projects_ok = False
    all_permissions_ok = False

    try:
        source_ltoken = kw_common.get_token(source_host, source_port, kw_user)
        target_ltoken = kw_common.get_token(target_host, target_port, kw_user)

        source_project_list = kw_common.get_projects(kw_user, source_ltoken,
                                                     source_api_url)
        target_project_list = kw_common.get_projects(kw_user, target_ltoken,
                                                     target_api_url)

        source_project_list.sort()
        target_project_list.sort()

        logger.info('Comparing project lists...')
        if cmp(source_project_list, target_project_list) == 0:
            logger.info('Klocwork projects are the same.')
            for project in source_project_list:
                project_name = project['name']
                logger.info('Validating {} project...'.format(project_name))
                if compare_builds(kw_user, source_ltoken, source_api_url,
                                  project_name, target_ltoken, target_api_url):
                    logger.info('Builds are identical for {}.'.format(project_name))
                    if compare_issues(kw_user, source_ltoken, source_api_url,
                                      project_name, target_ltoken, target_api_url):
                        logger.info('Issues are identical for {}.'.format(project_name))
                        if compare_checkers(kw_user, source_ltoken, source_api_url,
                                            project_name, target_ltoken, target_api_url):
                            logger.info('Checkers are identical for {}'
                                        .format(project_name))
                        else:
                            logger.error('{} Klocwork checkers do not match.'
                                         .format(project_name))
                            break
                    else:
                        logger.error('{} Klocwork issues do not match.'
                                     .format(project_name))
                        break
                else:
                    logger.error('{} Klocwork builds do not match.'.format(project_name))
                    break
            else:
                all_projects_ok = True
        else:
            logger.error('Klocwork project list does not match.')

        if all_projects_ok:
            all_permissions_ok = compare_permissions(kw_user, source_ltoken,
                                                     source_api_url, target_ltoken,
                                                     target_api_url)

        ret_val = 0 if all_projects_ok and all_permissions_ok else 1

    # Basic Exception Handling
    except HTTPError as he:
        logger.error('HTTPError {}'.format(he.code))
        logger.exception(he)

    except (URLError, OSError, IOError, ValueError) as e:
        logger.exception(e)

    except Exception as e:
        logger.error('Unhandled Exception')
        logger.exception(e)

    return ret_val


def compare_permissions(kw_user, source_ltoken, source_api_url,
                        target_ltoken, target_api_url):
    """

    @param kw_user:
    @param source_ltoken:
    @param source_api_url:
    @param target_ltoken:
    @param target_api_url:
    @return: (boolean)
    """
    logger.info('Validating permissions...')
    ret_val = False
    source_role_list = kw_common.get_roles(kw_user, source_ltoken, source_api_url)
    target_role_list = kw_common.get_roles(kw_user, target_ltoken, target_api_url)

    source_role_list.sort()
    target_role_list.sort()

    if cmp(source_role_list, target_role_list):
        logger.info('Klocwork roles are identical')
        logger.info('Comparing permissions for each role...')

        source_data = {'action': 'role_assignments',
                       'user': kw_user,
                       'ltoken': source_ltoken}

        target_data = {'action': 'role_assignments',
                       'user': kw_user,
                       'ltoken': target_ltoken}

        for role in source_role_list:
            logger.info('Comparing permissions for {} role...'.format(role))
            source_assignment_list = []
            source_data['search'] = '+{}'.format(role)
            source_encoded_data = urllib.urlencode(source_data)
            source_request = urllib2.Request(source_api_url, source_encoded_data)
            source_response = urllib2.urlopen(source_request)
            source_role = json.load(fp=source_response, encoding='utf-8')
            source_assignments = source_role['assignments']
            for assignment in source_assignments:
                if assignment['group'] and ('projectId' in assignment):
                    source_assignment_list.append(
                        {'project_id': assignment['projectId'],
                         'group_name': assignment['name']})

            target_assignment_list = []
            target_data['search'] = '+{}'.format(role)
            target_encoded_data = urllib.urlencode(target_data)
            target_request = urllib2.Request(target_api_url, target_encoded_data)
            target_response = urllib2.urlopen(target_request)
            target_role = json.load(fp=target_response, encoding='utf-8')
            target_assignments = target_role['assignments']
            for assignment in target_assignments:
                if assignment['group'] and ('projectId' in assignment):
                    target_assignment_list.append(
                        {'project_id': assignment['projectId'],
                         'group_name': assignment['name']})

            source_assignment_list.sort()
            target_assignment_list.sort()

            if not cmp(source_assignment_list, target_assignment_list) == 0:
                logger.error('Permission group mismatch in {} role'.format(role))
                break
        else:
            ret_val = True
    else:
        logger.error('Klocwork roles do not match.')

    return ret_val


def compare_checkers(kw_user, source_ltoken, source_api_url,
                     project, target_ltoken, target_api_url):
    """

    @param kw_user:
    @param source_ltoken:
    @param source_api_url:
    @param project:
    @param target_ltoken:
    @param target_api_url:
    @return: (boolean)
    """
    logger.info('Validating checkers for {} project...'.format(project))
    ret_val = False
    source_data = {'action': 'defect_types', 'user': kw_user,
                   'ltoken': source_ltoken, 'project': project}
    source_checker_list = []

    target_data = {'action': 'defect_types', 'user': kw_user,
                   'ltoken': target_ltoken, 'project': project}
    target_checker_list = []

    source_encoded_data = urllib.urlencode(source_data)
    source_request = urllib2.Request(source_api_url, source_encoded_data)
    source_response = urllib2.urlopen(source_request)
    for source_checker in source_response:
        source_checker = json.loads(source_checker, encoding='utf-8')
        source_checker_list.append({'code': source_checker['code'],
                                    'name': source_checker['name'],
                                    'status': str(source_checker['enabled']),
                                    'severity': str(source_checker['severity'])})

    target_encoded_data = urllib.urlencode(target_data)
    target_request = urllib2.Request(target_api_url, target_encoded_data)
    target_response = urllib2.urlopen(target_request)
    for target_checker in target_response:
        target_checker = json.loads(target_checker, encoding='utf-8')
        target_checker_list.append({'code': target_checker['code'],
                                    'name': target_checker['name'],
                                    'status': str(target_checker['enabled']),
                                    'severity': str(target_checker['severity'])})

    source_checker_list.sort()
    target_checker_list.sort()

    ret_val = cmp(source_checker_list, target_checker_list) == 0

    return ret_val


def compare_issues(kw_user, source_ltoken, source_api_url,
                   project, target_ltoken, target_api_url):
    """

    @param kw_user:
    @param source_ltoken:
    @param source_api_url:
    @param project:
    @param target_ltoken:
    @param target_api_url:
    @return: (boolean)
    """
    logger.info('Validating issues for {} project...'.format(project))
    ret_val = False
    source_data = {'action': 'report', 'user': kw_user, 'ltoken': source_ltoken,
                   'project': project, 'group_issues': True}

    target_data = {'action': 'report', 'user': kw_user, 'ltoken': target_ltoken,
                   'project': project, 'group_issues': True}

    source_encoded_data = urllib.urlencode(source_data)
    source_request = urllib2.Request(source_api_url, source_encoded_data)
    source_response = urllib2.urlopen(source_request)
    source_issues = json.load(fp=source_response, encoding='utf-8')
    source_issue_count = source_issues['data'][0][0]

    target_encoded_data = urllib.urlencode(target_data)
    target_request = urllib2.Request(target_api_url, target_encoded_data)
    target_response = urllib2.urlopen(target_request)
    target_issues = json.load(fp=target_response, encoding='utf-8')
    target_issue_count = target_issues['data'][0][0]

    ret_val = (source_issue_count == target_issue_count)

    return ret_val


def compare_builds(kw_user, source_ltoken, source_api_url,
                   project, target_ltoken, target_api_url):
    """

    @param kw_user:
    @param source_ltoken:
    @param source_api_url:
    @param project:
    @param target_ltoken:
    @param target_api_url:
    @return: (boolean)
    """
    logger.info('Validating builds for {} project...'.format(project))
    ret_val = False
    source_data = {'action': 'builds', 'user': kw_user, 'ltoken': source_ltoken,
                   'project': project}
    source_build_list = []

    target_data = {'action': 'builds', 'user': kw_user, 'ltoken': target_ltoken,
                   'project': project}
    target_build_list = []

    source_encoded_data = urllib.urlencode(source_data)
    source_request = urllib2.Request(source_api_url, source_encoded_data)
    source_response = urllib2.urlopen(source_request)
    for source_build in source_response:
        source_build = json.loads(source_build, encoding='utf-8')
        source_build_list.append({'id': source_build['id'],
                                  'name': source_build['name'],
                                  'date': source_build['date']})

    target_encoded_data = urllib.urlencode(target_data)
    target_request = urllib2.Request(target_api_url, target_encoded_data)
    target_response = urllib2.urlopen(target_request)
    for target_build in target_response:
        target_build = json.loads(target_build, encoding='utf-8')
        target_build_list.append({'id': target_build['id'],
                                  'name': target_build['name'],
                                  'date': target_build['date']})

    source_build_list.sort()
    target_build_list.sort()

    ret_val = (cmp(source_build_list, target_build_list) == 0)

    return ret_val


# Entry point
if __name__ == '__main__':
    exit_code = main()
    exit(exit_code)
